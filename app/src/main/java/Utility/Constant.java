package Utility;

public class Constant {

    public static String dbName = "GovernmentScheme.db";
    public static int dbVersion = 1;

    public static final String AdminEmailAddress="aswdc@darshan.ac.in";
    public static final String ASWDCEmailAddress="aswdc@darshan.ac.in";
    public static final String AppPlayStoreLink="https://play.google.com/store/apps/details?id=com.aswdc_gtu_mcq&hl=en";
//    public static final String SharedMessage="Download GTU MCQ App for B.E. 1st Year subject of GTU. Android: http://tiny.cc/agtumcq and  iPhone: http://tiny.cc/igtumcq";
    public static final String AdminMobileNo="+91-02822-293010";
    public static final String BASE_URL_CONTACT="http://api.aswdc.in/Api/MST_AppVersions/";
    public static final String app_key_feedback="1234";
    public static final int VERSION_CODE = 1;
}
