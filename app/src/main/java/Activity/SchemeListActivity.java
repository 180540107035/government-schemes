package Activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aswdc.govtschemes.R;

import java.util.ArrayList;

import Adapter.NameAdapter;
import Bean.BeanSubCategory;
import Database.DBSubCategory;

public class SchemeListActivity extends BaseActivity implements NameAdapter.ItemClickListener {

    RecyclerView nameRecyclerView;
    CardView nameCardView;
    NameAdapter nameAdapter;
    DBSubCategory db_subCategory;

    EditText etSearch;
    ArrayList<BeanSubCategory> arraySubCategory = new ArrayList<>();
    ArrayList<BeanSubCategory> tempArray = new ArrayList<>();


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scheme);

        String CategoryName = getIntent().getStringExtra("CategoryName");

        setupActionBar(CategoryName, true);

        int CategoryID = getIntent().getIntExtra("CategoryID", 0);
        db_subCategory = new DBSubCategory(this);


        nameRecyclerView = (RecyclerView) findViewById(R.id.nameRecyclerView);
        nameRecyclerView.setLayoutManager(new GridLayoutManager(this, 1));
        arraySubCategory.addAll(new DBSubCategory(this).selectName(CategoryID));
        nameCardView = (CardView) findViewById(R.id.nameCardView);
        etSearch = (EditText) findViewById(R.id.etSearch);



      /*  nameAdapter = new NameAdapter(this, arraySubCategory, this);
        nameRecyclerView.setAdapter(nameAdapter);
        nameAdapter.setClickListener();*/
        resetAdapter();

        tempArray.addAll(arraySubCategory);


        setSearch();
        checkAndVisibleView();


    }

    void resetAdapter() {
        if (nameAdapter != null) {
            nameAdapter.notifyDataSetChanged();
        } else {
            nameAdapter = new NameAdapter(this, tempArray, this);
            nameRecyclerView.setAdapter(nameAdapter);
            nameAdapter.setClickListener();

        }
    }

    void setSearch() {
        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                tempArray.clear();
                if (charSequence.toString().length() > 0) {
                    for (int j = 0; j < arraySubCategory.size(); j++) {
                        if (arraySubCategory.get(j).getSubCategoryName().contains(charSequence.toString())) {
                            tempArray.add(arraySubCategory.get(j));
                        }
                    }
                }
                if (tempArray.size() == 0 && charSequence.toString().length() == 0) {
                    tempArray.addAll(arraySubCategory);
                }
                resetAdapter();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }


    @Override
    public void onItemClick(int position) {

        Intent intent = new Intent(getApplicationContext(), SchemeDetailActivity.class);
        intent.putExtra("SchemeID", arraySubCategory.get(position).getSubCategoryID());
        intent.putExtra("SchemeName", arraySubCategory.get(position).getSubCategoryName());
        startActivity(intent);

    }


    public void checkAndVisibleView() {
        if (arraySubCategory.size() > 0) {

            nameRecyclerView.setVisibility(View.VISIBLE);
        } else {

            nameRecyclerView.setVisibility(View.GONE);
        }
    }


}