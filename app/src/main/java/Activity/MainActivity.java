package Activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aswdc.govtschemes.R;

import java.util.ArrayList;

import Adapter.TypeAdapter;
import Bean.BeanCategory;
import Database.DBCategory;

public class MainActivity extends BaseActivity implements TypeAdapter.ItemClickListener{

    RecyclerView typeRecyclerView;
    CardView typeCardView;
    TypeAdapter typeAdapter;
    DBCategory db_category;
    ArrayList<BeanCategory> arrayCategory;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle("સરકારી યોજના");
        setupActionBar("સરકારી યોજના",false);

        typeRecyclerView = (RecyclerView) findViewById(R.id.typeRecyclerView);
        typeRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        typeCardView=(CardView) findViewById(R.id.typeCard);

        db_category = new DBCategory(this);
        arrayCategory = new ArrayList<>();

        arrayCategory= db_category.selectAll();

        typeAdapter = new TypeAdapter(this, arrayCategory, this);
        typeRecyclerView.setAdapter(typeAdapter);
        typeAdapter.setClickListener();



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_menu,menu);
        return super.onCreateOptionsMenu(menu);


    }
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case R.id.iDevelopers:
                Intent intent = new Intent(this, DeveloperActivity.class);
                this.startActivity(intent);
                break;
            case R.id.iFeedback:
                Intent intent1 = new Intent(this, ActivityFeedback.class);
                this.startActivity(intent1);
                break;
            case R.id.iMoreApps:
                Intent moreappsintent = new Intent("android.intent.action.VIEW", Uri.parse("market://search?q=pub:Darshan+Institute+of+Engineering+%26+Technology"));
                startActivity(moreappsintent);
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;

//        Intent intent = new Intent(MainActivity.this, DeveloperActivity.class);
//        startActivity(intent);
//        return super.onOptionsItemSelected(item);

    }


    @Override
    public void onItemClick( int position) {

        Intent intent = new Intent(getApplicationContext(),SchemeListActivity.class);
        intent.putExtra("CategoryID",arrayCategory.get(position).getCategoryID());
        intent.putExtra("CategoryName",arrayCategory.get(position).getCategoryName());
        startActivity(intent);

    }

    boolean doubleBackToExitPressedOnce = false;

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }

}