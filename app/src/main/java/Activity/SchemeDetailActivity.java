package Activity;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.aswdc.govtschemes.R;

import java.util.ArrayList;

import Adapter.SchemeAdapter;
import Bean.BeanScheme;
import Database.DBScheme;

public class SchemeDetailActivity extends BaseActivity {

    ArrayList<BeanScheme> arrayScheme = new ArrayList<>();
    DBScheme db_Scheme;
    RecyclerView detailRecyclerView;
    SchemeAdapter schemeAdapter;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scheme_detail);

        String SubCategoryName = getIntent().getStringExtra("SchemeName");

        setupActionBar(SubCategoryName, true);

        int SubCategoryID = getIntent().getIntExtra("SchemeID", 0);
        db_Scheme = new DBScheme(this);

        detailRecyclerView = (RecyclerView)findViewById(R.id.detailrecyclerView);
        arrayScheme = new ArrayList<>();
        arrayScheme.clear();
        arrayScheme.addAll(new DBScheme(this).selectScheme(SubCategoryID));
        schemeAdapter = new SchemeAdapter(this,arrayScheme);
        detailRecyclerView.setAdapter(schemeAdapter);
    }


}
