package Database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.util.ArrayList;

import Bean.BeanSubCategory;
import Bean.BeanCategory;
import Utility.Constant;

public class DBCategory extends SQLiteAssetHelper {

    public DBCategory(Context context) {
        super(context, Constant.dbName, null, null, Constant.dbVersion);
    }

    public ArrayList<BeanCategory> selectAll() {

        SQLiteDatabase database = getReadableDatabase();
        String strQuery = " SELECT * FROM Category";

        Cursor cursor = database.rawQuery(strQuery, null);

        ArrayList<BeanCategory> arrayCategory = new ArrayList<BeanCategory>();

        if (cursor.moveToFirst()) {
            do {
                BeanCategory beanCategory = new BeanCategory();
                beanCategory.setCategoryID(cursor.getInt(cursor.getColumnIndex("CategoryID")));
                beanCategory.setCategoryName(cursor.getString(cursor.getColumnIndex("CategoryName")));
                beanCategory.setCategoryImage(cursor.getBlob(cursor.getColumnIndex("CategoryImage")));
                arrayCategory.add(beanCategory);
            }
            while (cursor.moveToNext());
        }
        database.close();
        return arrayCategory;
    }

    public ArrayList<BeanSubCategory> selectById(int categoryID) {

        SQLiteDatabase database = getReadableDatabase();
        String strQuery = " Select SubCategoryName From SubCategory sub Inner Join Category cat on sub.CategoryID=cat.CategoryID where cat.CategoryID="+categoryID;

        Cursor cursor = database.rawQuery(strQuery, null);

        ArrayList<BeanSubCategory> arraySubCategory = new ArrayList<BeanSubCategory>();

        if (cursor.moveToFirst()) {
            do {
                BeanSubCategory beanSubCategory = new BeanSubCategory();
                beanSubCategory.setCategoryID(cursor.getInt(cursor.getColumnIndex("CategoryID"+"")));
                beanSubCategory.setSubCategoryID(cursor.getInt(cursor.getColumnIndex("SubCategoryID")));
                beanSubCategory.setSubCategoryName(cursor.getString(cursor.getColumnIndex("SubCategoryName")));

                arraySubCategory.add(beanSubCategory);
            }
            while (cursor.moveToNext());
        }
        database.close();
        return arraySubCategory;
    }


    
}
