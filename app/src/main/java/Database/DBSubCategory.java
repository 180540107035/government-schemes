 package Database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.util.ArrayList;

import Bean.BeanSubCategory;
import Utility.Constant;

public class DBSubCategory extends SQLiteAssetHelper {

    public DBSubCategory(Context context) {
        super(context, Constant.dbName, null, Constant.dbVersion);
    }

    public ArrayList<BeanSubCategory> selectName(int categoryID){

        SQLiteDatabase database =getReadableDatabase();
        String strQuery = "Select " +
                " SubCategoryID," +
                " SubCategoryName,"+
                " sub.CategoryID " +
                " from SubCategory sub Inner Join Category cat on sub.CategoryID=cat.CategoryID where cat.CategoryID="+categoryID;

        Cursor cursor = database.rawQuery(strQuery,null);
        ArrayList<BeanSubCategory> arraySubCategory = new ArrayList<BeanSubCategory>();

        if(cursor.moveToFirst()){
            do{
                BeanSubCategory beanSubCategory = new BeanSubCategory();
                beanSubCategory.setSubCategoryID(cursor.getInt(cursor.getColumnIndex("SubCategoryID")));
                beanSubCategory.setSubCategoryName(cursor.getString(cursor.getColumnIndex("SubCategoryName")));
                beanSubCategory.setCategoryID(cursor.getInt(cursor.getColumnIndex("CategoryID")));
                arraySubCategory.add(beanSubCategory);
            }
            while (cursor.moveToNext());
        }
        database.close();
        return arraySubCategory;

    }
}
