package Database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import Bean.BeanScheme;
import Bean.BeanSubCategory;
import Utility.Constant;

public class DBScheme extends SQLiteAssetHelper {

    public static String dbName = "GovernmentScheme.db";
    public static int dbVersion = 1;

    public DBScheme(Context context) {
        super(context, dbName, null, null, dbVersion);
    }

    public ArrayList<BeanScheme> selectScheme(int subCategoryID) {

        SQLiteDatabase database = getReadableDatabase();

        String strQuery = "Select * from Scheme where SubCategoryID="+subCategoryID;

        Cursor cursor = database.rawQuery(strQuery, null);
        //  ArrayList<BeanDetail> arrayScheme = new ArrayList<BeanDetail>();
        ArrayList<BeanScheme> arrayList = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                int i = cursor.getColumnCount();
                for (int j = 0; j < i; j++) {
                    BeanScheme beanScheme = new BeanScheme();
                    beanScheme.setTitle(cursor.getColumnName(j));
                    beanScheme.setName(cursor.getColumnName(j));
                    beanScheme.setDescription(cursor.getString(cursor.getColumnIndex((cursor.getColumnName(j)))));
                    if (beanScheme.getDescription() != null && (!cursor.getColumnName(j).equalsIgnoreCase("SchemeID"))
                            && (!cursor.getColumnName(j).equalsIgnoreCase("SchemeName"))
                            && (!cursor.getColumnName(j).equalsIgnoreCase("SubCategoryID"))) {
                        arrayList.add(beanScheme);
                    }
                }
            }
            while (cursor.moveToNext());
        }
        database.close();
        return arrayList;

    }

}


