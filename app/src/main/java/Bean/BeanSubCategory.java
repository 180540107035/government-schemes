package Bean;

import java.io.Serializable;

public class BeanSubCategory implements Serializable {

    private int SubCategoryID;
    private String SubCategoryName;
    private int CategoryID;

    public int getSubCategoryID() {
        return SubCategoryID;
    }

    public void setSubCategoryID(int subCategoryID) {
        SubCategoryID = subCategoryID;
    }

    public String getSubCategoryName() {
        return SubCategoryName;
    }

    public void setSubCategoryName(String subCategoryName) {
        SubCategoryName = subCategoryName;
    }

    public int getCategoryID() {
        return CategoryID;
    }

    public void setCategoryID(int categoryID) {
        CategoryID = categoryID;
    }
}
