package Bean;

import java.io.Serializable;

public class BeanScheme implements Serializable {
    String title;
    String Name;
    String description;
    private boolean expanded;

    public BeanScheme() {
        this.expanded = false;
    }
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isExpanded() {
        return expanded;
    }

    public void setExpanded(boolean expanded) {
        this.expanded = expanded;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {

        switch (name){
            case "Who":
                Name = "લાભ કોને મળે";
                break;
            case "How":
                Name = "કેટલો લાભ મળે";
                break;
            case "FromWhere":
                Name = "લાભ ક્યાંથી મળે";
                break;
            case "Doc":
                Name = "ક્યાં ક્યાં પુરાવા જોઈ";
                break;
            case "Note" :
                Name = "નોંધ";
                break;
            case "Steps":
                Name ="યોજનાનો લાભ મેળવા માટેની પદ્ધતિ";
                break;
            case "Conditions":
                Name = "શરતો";
                break;
            case "DocToOpenAccount":
                Name = "ખાતુ ખોલવા ક્યાં ક્યાં પુરાવા જોઈ";
                break;
            case "Udvesh":
                Name = "ઉદ્વેશ";
                break;
            case "CommunityThemes":
                Name = "સમુદાય આધારિત પ્રસંગો";
                break;
            case "CampaignActivities":
                Name = "પોષણ અભિયાન અંતર્ગત કરવામાં આવતી પ્રવૃતિઓ";
                break;
            case "EvidenceToClaim":
                Name = "ક્લેમ કરવા માટે જરૂરી પુરાવા";
                break;
            case "AmountOfAssistance":
                Name = "સહાયની રકમ";
                break;
            case "WhoWillPayAid":
                Name = "સહાય કોણ ચૂકવશે અને કેવી રીતે ચૂકવશે?";
                break;
            case "Implementation":
                Name = "યોજનાનું અમલીકરણ";
                break;
            case "PaymentMethod":
                Name ="ચુકવણીની પદ્ધતિ";
                break;
            case "ScholarshipRate":
                Name ="શિષ્યવૃતિનો દર";
                break;
            case "WhereToApply":
                Name = "અરજી ક્યાંથી કરવી";
                break;
            case "EvidenceForApplication":
                Name = "અરજી સાથે જોડવાના પુરાવા";
                break;
            case "RateOfInterest":
                Name = "વ્યજનો દર";
                break;
            case "ForChild":
                Name = "નવજાત શિશુને 1 વર્ષ સુધી મળવા પાત્ર લાભ";
                break;
            case "TollFree":
                Name = "ટોલફ્રી નંબર";
                break;
            case "SelectionOfCandidate":
                Name="લાભરથીના પસંદગીના નિયમો";
                break;
            case "HelpOfMerchant":
                Name= "મર્જિમની સહાય";
                break;
            case "ImplementationAgency":
                Name="અમલીકરણ એજન્સી";
                break;
            case "FinancialInstitutions":
                Name="નાણાકીય સંસ્થાઓ";
                break;
            case "RepaymentOfLoan":
                Name = "લોનની પરત ભરપાય";
                break;
            case "AboutScheme":
                Name="યોજના વિશે";
                break;
            case "Ghatak":
                Name="ઘટક";
                break;
            case "Varasdaro":
                Name = "વરસાદરો";
                break;
            case "Dhiran":
                Name = "મુદ્દતી ધીરન";
                break;
        }
    }
}
