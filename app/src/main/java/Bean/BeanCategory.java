package Bean;

import java.io.Serializable;

public class BeanCategory implements Serializable {

    private int CategoryID;
    private String CategoryName;
    private byte[] CategoryImage;

    public int getCategoryID() {
        return CategoryID;
    }

    public void setCategoryID(int categoryID) {
        CategoryID = categoryID;
    }

    public String getCategoryName() {
        return CategoryName;
    }

    public void setCategoryName(String categoryName) {
        CategoryName = categoryName;
    }

    public byte[] getCategoryImage() {
        return CategoryImage;
    }

    public void setCategoryImage(byte[] categoryImage) {
        CategoryImage = categoryImage;
    }


}
