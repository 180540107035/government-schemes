package Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.aswdc.govtschemes.R;

import java.util.ArrayList;

import Bean.BeanSubCategory;

public class NameAdapter extends RecyclerView.Adapter<NameAdapter.ViewHolder>implements Filterable {

    Context context;
    private ArrayList<BeanSubCategory> arraySubCategory;
    private LayoutInflater inflater;
    NameAdapter.ItemClickListener itemClickListener;
    View.OnClickListener onClickListener;
    ArrayList<BeanSubCategory> tempArray;

    // data is passed into the constructor
    public NameAdapter(Context context, ArrayList<BeanSubCategory> arraySubCategory, NameAdapter.ItemClickListener itemClickListener) {
        this.inflater = LayoutInflater.from(context);
        this.arraySubCategory = arraySubCategory;
        this.itemClickListener=itemClickListener;
        tempArray=new ArrayList<>(arraySubCategory);

    }

    // inflates the row layout from xml when needed
    @Override
    public NameAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.list_row_scheme, parent, false);
        view.setOnClickListener(onClickListener);
        return new NameAdapter.ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(NameAdapter.ViewHolder holder, int position) {

        holder.subCategoryID.setText(Integer.toString(arraySubCategory.get(position).getSubCategoryID()));
        holder.categoryID.setText(Integer.toString(arraySubCategory.get(position).getCategoryID()));
        holder.subCategoryName.setText(arraySubCategory.get(position).getSubCategoryName());


        holder.llNameList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (itemClickListener != null)
                    itemClickListener.onItemClick(position);

            }
        });
    }

    // total number of rows
    @Override
    public int getItemCount() {
        return arraySubCategory.size();
    }

    @Override
    public Filter getFilter() {
        return filter;
    }

    Filter filter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence keyword) {
            ArrayList<BeanSubCategory> filterdata = new ArrayList<>();
            if (keyword.toString().isEmpty()) {
                filterdata.addAll(tempArray);
            } else {
                for (BeanSubCategory object : tempArray) {
                    if (object.getSubCategoryName().toLowerCase().contains(keyword.toString().toLowerCase())) {
                        filterdata.add(object);
                    }
                }
            }
            FilterResults results = new FilterResults();
            results.values = filterdata;
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            arraySubCategory.clear();
            arraySubCategory.addAll((ArrayList<BeanSubCategory>) results.values);
            notifyDataSetChanged();
        }
    };
    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView subCategoryName;
        TextView subCategoryID,categoryID;
        LinearLayout llNameList;

        ViewHolder(View itemView) {
            super(itemView);
            context = itemView.getContext();
            subCategoryName=(TextView)itemView.findViewById(R.id.schemeNameList);
            subCategoryID = (TextView)itemView.findViewById(R.id.schemeNameId);
            categoryID=(TextView)itemView.findViewById(R.id.schemeNameTypeId);
            llNameList=(LinearLayout)itemView.findViewById(R.id.llNameList);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (itemClickListener != null)
                itemClickListener.onItemClick(getAdapterPosition());

        }
    }

    // convenience method for getting data at click position
    public BeanSubCategory getItem(int id) {
        return arraySubCategory.get(id);
    }

    // allows clicks events to be caught
    public void setClickListener() {
        this.itemClickListener = itemClickListener;
    }



    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(int position);
    }


}

