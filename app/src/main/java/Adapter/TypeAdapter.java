package Adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.aswdc.govtschemes.R;

import java.util.ArrayList;

import Bean.BeanCategory;

public class TypeAdapter extends RecyclerView.Adapter<TypeAdapter.ViewHolder> {


         Context context;
         private ArrayList<BeanCategory> arrayCategory;
         private LayoutInflater inflater;
         ItemClickListener itemClickListener;
         View.OnClickListener onClickListener;

        // data is passed into the constructor
        public TypeAdapter(Context context, ArrayList<BeanCategory> arrayCategory, ItemClickListener itemClickListener) {
            this.inflater = LayoutInflater.from(context);
            this.arrayCategory = arrayCategory;
            this.itemClickListener=itemClickListener;

        }

        // inflates the row layout from xml when needed
        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = inflater.inflate(R.layout.list_row_type, parent, false);
            view.setOnClickListener(onClickListener);
            return new ViewHolder(view);
        }

        // binds the data to the TextView in each row
        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {

            holder.categoryID.setText(Integer.toString(arrayCategory.get(position).getCategoryID()));
            holder.categoryName.setText(arrayCategory.get(position).getCategoryName());
            byte[] categoryImage = arrayCategory.get(position).getCategoryImage();
            Bitmap bitmap = BitmapFactory.decodeByteArray(categoryImage,0,categoryImage.length);
            holder.categoryImage.setImageBitmap(bitmap);

            holder.typeCardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (itemClickListener != null)
                        itemClickListener.onItemClick(position);

                }
            });
        }

        // total number of rows
        @Override
        public int getItemCount() {
            return arrayCategory.size();
        }


    // stores and recycles views as they are scrolled off screen
        public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
            TextView categoryName,categoryID;
            CardView typeCardView;
            ImageView categoryImage;


            ViewHolder(View itemView) {
                super(itemView);
                context = itemView.getContext();
                typeCardView = itemView.findViewById(R.id.typeCard);
                categoryName = itemView.findViewById(R.id.typeText);
                categoryID = itemView.findViewById(R.id.typeId);
                categoryImage = itemView.findViewById(R.id.typeImage);
                itemView.setOnClickListener(this);
            }

            @Override
            public void onClick(View view) {
                if (itemClickListener != null)
                    itemClickListener.onItemClick(getAdapterPosition());

            }
        }

        // convenience method for getting data at click position
        public BeanCategory getItem(int id) {
            return arrayCategory.get(id);
        }

        // allows clicks events to be caught
        public void setClickListener() {
            this.itemClickListener = itemClickListener;
        }



        // parent activity will implement this method to respond to click events
        public interface ItemClickListener {
            void onItemClick(int position);
        }


    }





