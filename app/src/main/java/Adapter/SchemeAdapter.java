 package Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.aswdc.govtschemes.R;

import java.util.ArrayList;

import Bean.BeanScheme;

public class SchemeAdapter extends RecyclerView.Adapter<SchemeAdapter.ViewHolder> {

    Context context;
    ArrayList<BeanScheme> arrayScheme ;

    public SchemeAdapter(Context context, ArrayList<BeanScheme> arrayScheme) {
        this.context = context;
        this.arrayScheme = arrayScheme;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_row_detail,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.tvTitle.setText(arrayScheme.get(position).getName());
        holder.textWho.loadDataWithBaseURL(null,"<html><head><style type=\"text/css\"> li{\n" +
                "    margin: 0;\n" +
                "    padding: 0;\n" +
                "    border: 0;\n" +
                "}</style></head><body>"+arrayScheme.get(position).getDescription()+"</body></html>","text/html", "utf-8",null);
        boolean isExpanded = arrayScheme.get(position).isExpanded();
        holder.expandableLayout.setVisibility(isExpanded ? View.VISIBLE : View.GONE);
        holder.expandArrow.setImageResource(isExpanded ? R.drawable.upgreyarrow : R.drawable.downgreyarrow);
    }


    @Override
    public int getItemCount() {
        return arrayScheme.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvTitle;
        LinearLayout llmain;
        WebView textWho;
        LinearLayout expandableLayout;
        ImageView expandArrow;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvTitle = (TextView)itemView.findViewById(R.id.tvTitle);
            textWho = (WebView)itemView.findViewById(R.id.textWho);
            expandableLayout = (LinearLayout)itemView.findViewById(R.id.expandableLayout);
            llmain = (LinearLayout)itemView.findViewById(R.id.llmain);
            expandArrow = (ImageView)itemView.findViewById(R.id.expandArrow);

            llmain.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    BeanScheme beanScheme = arrayScheme.get(getAdapterPosition());
                    beanScheme.setExpanded(beanScheme.isExpanded()?false:true);
                    notifyItemChanged(getAdapterPosition());
                }
            });
        }
    }
}
//holder.textWho.setText(Html.fromHtml(""+arrayScheme.get(position).getDescription())+"");